import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiServiceProvider {

    header: HttpHeaders;
    static baseUrl: string = 'http://54.147.244.100/api/';
    private token:string = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';

    constructor(private http: HttpClient) {
        this.header     = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+this.token
        });
    }

    getMsgErrorHttp(errorResponse:any) {
        let message = '';
        switch(errorResponse.status) {
            case 400:
                message = errorResponse.error.message;
                break;
            case 401:
                message = errorResponse.error.message;
                break;
            case 404:
                message = errorResponse.error.message;
                break;
            case 409:
                message = errorResponse.error.message;
                break;
            case 409:
                message = errorResponse.error.message;
                break;
            case 422:
                if(errorResponse.error.hasOwnProperty('errors')) {
                    let errors:any = errorResponse.error.errors;
                    for(let error in errors) {
                        message = errors[error][0];
                        break;
                    }
                    
                    break;
                }
                message = errorResponse.error.message;
                break;
            default:
                message = 'Algo inesperado aconteceu, tente novamente!';
        }

        return message;
    }

}
