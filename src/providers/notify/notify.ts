import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Injectable()
export class NotifyProvider {

    constructor(
        public alertCtrl: AlertController
    ) {
        // empty
    }

    showMessage(message:any) {
        let alert = this.alertCtrl.create({
            title: message.title,
            subTitle: message.subTitle,
            buttons: ['OK']
        });

        alert.present();
    }

    showConfirm(message:any, successCallback) {
        let alert = this.alertCtrl.create({
            title: message.title,
            message: message.subTitle,
            buttons: [
                {
                    text: 'Confirmar',
                    role: 'confirm',
                    handler: successCallback
                },
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: () => false
                }
            ]
        });

        alert.present();
    }

}
