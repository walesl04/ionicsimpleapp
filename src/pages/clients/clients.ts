import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

// Services
import { ClientServiceProvider } from '../../providers/client-service/client-service';
import { NotifyProvider } from '../../providers/notify/notify';

// Models
import { Client } from '../../models/clients/client';

// Page
import { ClientModalPage } from '../client-modal/client-modal';

@IonicPage()
@Component({
  selector: 'page-clients',
  templateUrl: 'clients.html',
})
export class ClientsPage {

    clients:Array<Client> = [];

    constructor(
        public navCtrl:         NavController,
        private clientService:  ClientServiceProvider,
        public navParams:       NavParams,
        private modalCtrl:      ModalController,
        private notify:         NotifyProvider
    ) {
        // empty
    }

    ionViewDidLoad() {
        this.listClients();
    }

    listClients() {;
        this.clientService.list().subscribe((response:any) => {
            
            if(!response.hasOwnProperty('data')) {
                return false;
            }

            this.clients = response.data;

        }, (response:any) => {
            this.clientService.anyError(response);
        });
    }

    modalAdd() {
        const modal = this.modalCtrl.create(ClientModalPage);
        modal.present();
        modal.onDidDismiss( () => this.listClients() );
    }

    modalUpdate(client:Client) {
        if(!client) {
            return false;
        }

        const modal = this.modalCtrl.create(ClientModalPage, {
            client: client
        });
        modal.present();
        modal.onDidDismiss( () => this.listClients() );
    }

    delete(id:number) {
        if(id <= 0) {
            this.notify.showMessage({
                title: 'Identificação inválida!',
                subTitle: ''
            });
            return false;
        }

        const remove = (id:number) => {
            this.clientService.remove(id).subscribe((response:any) => {
                this.notify.showMessage({
                    title: 'Cliente Excluído!',
                    subTitle: ''
                });
                this.listClients();

            }, (response) => {
                const msgError:string = this.clientService.anyError(response);
                const message:any = {
                    title: 'Erro ao Atualizar Cliente!',
                    subTitle: msgError
                };
                this.notify.showMessage(message);
            });
        };

        this.notify.showConfirm({
            title: 'Deseja excluir o cliente?',
            message: 'Não será possível desfazer esta ação!'
        }, () => {
            remove(id);
        });
    }

}
