import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { ClientsPage } from '../pages/clients/clients';
import { ClientModalPage } from '../pages/client-modal/client-modal';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { ClientServiceProvider } from '../providers/client-service/client-service';
import { NotifyProvider } from '../providers/notify/notify';

@NgModule({
  declarations: [
    MyApp,
    ClientsPage,
    ClientModalPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ClientsPage,
    ClientModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiServiceProvider,
    ClientServiceProvider,
    NotifyProvider
  ]
})
export class AppModule {}
